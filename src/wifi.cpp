/* ***************************************************************************
* File:    wifi.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the GNU General Public License 3 (or, at your option. any later version)
*
* ******************************************************************************/

/* ---

--------------------------------------------------------------------------
### WIFI API
Provides wifi or hotspot access and enables over-the-aor updates.
--- */

#include "wifi.h"
#include "credentials.h"
#include "filesys.h"

#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <functional>

ESP8266WiFiMulti wifiMulti;		   // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'
const char *mdnsRoot = "eyeball"; // Domain name for the mDNS responder
static char mdnsName[32];		   // make it a composite of 'screamzy' and the last hex value from the MAC address

static bool _wifiConnected = false;
static bool _wifiHotspot = false;

#define WIFI_CHECK_INTERVAL 90000 // 90 seconds
static unsigned long g_wifi_fake_interupt_timer = 0;
static bool error_reported = false;

static bool startNetwork() { // Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
	// int channel = random(12) + 1;
	uint8_t addr[6];
	char hotspot_name[32];
	bool state = true;
	int i = 0;

	_wifiConnected = false;
	_wifiHotspot = false;

	WiFi.softAPmacAddress(addr);
	sprintf(hotspot_name, "%s_%02x%02x%02x", mdnsRoot, addr[3], addr[4], addr[5]);
	sprintf(mdnsName, "%s%02x", mdnsRoot, addr[5]);

	// add as many networks as you want ...
	wifiMulti.addAP(g_networkSSID, g_networkPassword); // add Wi-Fi networks you want to connect to when available
#ifdef CREDS2
	wifiMulti.addAP(g_networkSSID2, g_networkPassword2);
#endif
#ifdef CREDS3
	wifiMulti.addAP(g_networkSSID3, g_networkPassword3);
#endif
#ifdef CREDS4
	wifiMulti.addAP(g_networkSSID4, g_networkPassword4);
#endif

	Serial.print("Connecting ... ");
	while (wifiMulti.run() != WL_CONNECTED && WiFi.softAPgetStationNum() < 1) { // Wait for a Wi-Fi connection
		Serial.print(".");

		// we will attempt for 10 seconds
		delay(500);
		if (i++ > 20) {
			state = false;
			break;
		}
	}
	Serial.println("\n");

	if (state) {
		Serial.printf("%s connected to %s as %s\n", mdnsRoot, WiFi.SSID().c_str(), wifiAddress());
		_wifiConnected = true;
	} else if (WiFi.softAP(hotspot_name /*, NULL, channel */)) {
		// TODO - we want to supported a web interface to setup wifi SSID and PW so we fallback to being a hotspot to give access for setup steps
		// Start the access point with no password
		Serial.printf("established a hotspot as %s\n", hotspot_name);
		_wifiHotspot = true;
		_wifiConnected = true;
	} else {
		Serial.printf("failed to connect to an access point and failed to establish a hotspot as %s\n", hotspot_name);
	}
	return _wifiConnected;
}

static void startMDNS() { // Start the mDNS responder
	MDNS.begin(mdnsName); // start the multicast domain name server
	Serial.printf("mDNS responder started: http://%s.local\n", mdnsName);
}

/* ---
#### wifiInit()
Performs all necessary initialization. Must be called once before using any of the other wifi functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool wifiInit() {
	_wifiConnected = startNetwork();// Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
	startMDNS();					// Start the mDNS responder
	if (_wifiConnected) {
		error_reported = false;		// reset our reporting flag
		g_wifi_fake_interupt_timer = millis() + WIFI_CHECK_INTERVAL;
	}
	return _wifiConnected;
}

void wifiLoop() {
	// we check the wifi connection every 30 seconds
	// this will automatically deal with 2 cases
	// the first case is when we boot after a power failure and are target WiFi is not yet available
	// the second case is where our target WiFi hs gone down and is temporarily not available

	if (g_wifi_fake_interupt_timer > millis())
		return;

	g_wifi_fake_interupt_timer = millis() + WIFI_CHECK_INTERVAL;

	// if we never have connected, are we started up as a hotspot, we try again now
	if (!wifiIsConnected() || wifiIsHotspot())
		wifiInit();
	else if (wifiConnectionLost()) {
		if (!error_reported) {
			filesysLogPrint(LOG_ERROR, "WiFi Connection Lost");
			error_reported = true;	// dont keep repeating teh same error but do report it if it reoccurs
		}
		_wifiConnected = false;
		_wifiHotspot = false;
		wifiInit();
	}
}

/* ---
#### wifiIsConnected()
get the current availability of wifi connectivity - either via an existing access point or as a hotspot
 - return: **bool** `true` when wifi is available and `false` when not
--- */
bool wifiIsConnected() {
	return _wifiConnected;
}

/* ---
#### wifiConnectionLost()
test is we still have wifi connectivity; if we never had wifi we can't lose somethign we never had
 - return: **bool** `true` if we lost a prior wifi connection and `false` if we are at the same state as before
--- */
bool wifiConnectionLost() {
	// we were connected but are no longer connected
	return (_wifiConnected && (!_wifiHotspot) && (WiFi.status() != WL_CONNECTED));
}

/* ---
#### wifiIsHotspot()
indicate if teh wifi connection is a hotspot or connected to an access point
 - return: **bool** `true` when wifi is a hotspote and `false` when it is an access point
--- */
bool wifiIsHotspot() {
	return _wifiHotspot;
}

/* ---
#### wifiAddress()
a conveniece function to get the WiFi address as character string
 - return: **char pointer** static internal buffer of the wifi address in the form nnn.nnn.nnn.nnn
--- */
char *wifiAddress() {
#define IPADDRSIZE 16
	static char buffer[IPADDRSIZE + 1];
	IPAddress ip = WiFi.localIP();
	snprintf(buffer, IPADDRSIZE, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
	return buffer;
}

/* ---
#### wifiAddressEnding()
a conveniece function to get the last value in the WiFi address
 - return: **uint8_t** last number in wifi address eg xxx.xxx.xxx.NNN
--- */
uint8_t wifiAddressEnding() {
	IPAddress ip = WiFi.localIP();
	return ip[3];
}

#define OTA_PORT 8266 // its the default :-)

/* ---
#### otaInit()
Performs all necessary initialization. Must be called once before using any of the other OAT functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool otaInit() {
	if (_wifiConnected) {
		ArduinoOTA.setPort(OTA_PORT);
		ArduinoOTA.setHostname(g_otaHostname);
		ArduinoOTA.setPassword(g_otaPassword);

		ArduinoOTA.onStart([]() {
			String type;
			if (ArduinoOTA.getCommand() == U_FLASH) {
				type = "sketch";
			} else { // U_SPIFFS
				type = "filesystem";
			}
			// we reset the brightness in case some current display mode has dimmed (or cranked) the brightness
			//ledsSetBrightness(LEDS_DEFAULT_BRIGHTNESS);
			// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
			Serial.println("Start OTA update of " + type);
		});

		ArduinoOTA.onEnd([]() {
			Serial.println("\nEnd OTA update");
		});

		ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
			static uint8_t toggle = 0;

			// blick the onboard LED
			digitalWrite(2, toggle);

			// since we know we have RGB LEDs we can show the progress

#if 0 // this is a wig-wag which is useful when there are only two LEDs
			uint8_t color;
			color = 31 * toggle;
			ledsSetPixelRGB(0, 0, color, 0);
			color = 31 * ((toggle + 1) % 2);
			ledsSetPixelRGB(1, 0, color, 0);
			ledsShow();
			toggle = (toggle + 1) % 2;
			Serial.printf("Progress of OTA update: %u%%\r", (progress / (total / 100)));
#endif
#if 0 // this is a progressive fill for when we have a lot of LEDS
			int count = LEDS_COUNT * progress / total;
			for (int i = 0; i < count; i++)
				ledsSetPixelRGB((LEDS_COUNT - 1) - i, 15, 63, 15); // a greenish color
			ledsShow();
#endif
		});

		ArduinoOTA.onError([](ota_error_t error) {
			Serial.printf("OTA update error[%u]: ", error);
			if (error == OTA_AUTH_ERROR) {
				Serial.println("Auth Failed");
			} else if (error == OTA_BEGIN_ERROR) {
				Serial.println("Begin Failed");
			} else if (error == OTA_CONNECT_ERROR) {
				Serial.println("Connect Failed");
			} else if (error == OTA_RECEIVE_ERROR) {
				Serial.println("Receive Failed");
			} else if (error == OTA_END_ERROR) {
				Serial.println("End Failed");
			}
		});
		ArduinoOTA.begin();
		return true;
	}
	return false;
}

/* ---
#### otaLoop()
Give the OTA an opportunity to respond to over-the-air update requests
--- */
void otaLoop() {
	if (_wifiConnected) {
#if 0
		static bool _allow_ota = true;
 		if (_allow_ota) {
			// only allow OTA during the first 30 seconds; this prevents loop congestion which messes up Alexa access
			if (millis() > 30000)
				_allow_ota = false;
		}
#endif

		static unsigned long _test_cycle = 0;

		// we allow testing for OTA periodically
		if (millis() > _test_cycle) {
			ArduinoOTA.handle();
			_test_cycle += 2000; // 2 seconds from now
		}
	}
}
