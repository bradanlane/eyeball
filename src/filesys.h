#ifndef __FILE_SYSTEM_H
#define __FILE_SYSTEM_H

#include <FS.h>

#define LOG_MAXLEN 256

#define LOG_CRITICAL 0
#define LOG_ERROR 1
#define LOG_WARNING 2
#define LOG_STATISTICS 3
#define LOG_INFO 4
#define LOG_DEBUG 5
#define LOG_NONE 99

#ifndef MAX_LOG_LEVEL
#pragma GCC error "MAX_LOG_LEVEL must be defined as one of the log levels from filesys.h"
//#define MAX_LOG_LEVEL LOG_INFO
#endif

bool filesysInit();
bool filesysLoop();

bool filesysExists(String name);
File filesysOpen(String name);
void filesysClose(File handle);

bool filesysSaveStart(String name);
bool filesysSaveWrite(uint8_t* buf, size_t size);
bool filesysSaveFinish();

void filesysLogPrintf(int level, const char *fmt...);
void filesysLogPrint(int level, const char *msg);

#endif
