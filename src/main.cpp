/* ***************************************************************************
* File:    main.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the GNU General Public License 3 (or, at your option. any later version)
*
* ******************************************************************************/



/* ---
# Eye Ball

The Eye Ball is a fun - _some might say frivolous_ - toy which shows an eye which blinks.

It also could have other features such as using a 9DOF and added magic 8-ball capability.

--------------------------------------------------------------------------
--- */

// ![3D Printed Eye Ball with embedded electronics](https://gitlab.com/bradanlane/eyeball/raw/master/files/eyeball_small.png)



#include "filesys.h"
#include "wifi.h"
#include "web.h"
#include "eye.h"

#include <functional>





// -----------------------------------------------------------
// the standard Arduino entrypoints setup() and loop()
// -----------------------------------------------------------

void setup() {
	Serial.begin(115200);
	Serial.println("");
	Serial.println("");
	randomSeed(millis());

	filesysInit();
	wifiInit();
	otaInit();		// used wifi
	webInit();
	eyeInit();

	filesysLogPrint(LOG_INFO, "---- System Initialized ----\n");
	delay(50);
}

void loop() {
	wifiLoop();
	otaLoop();
	filesysLoop();
	webLoop();
	eyeLoop();

	delay(0);
}
