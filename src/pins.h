#ifndef __PINS_H
#define __PINS_H

/* ***************************************************************************
* File:    pins.h
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
* ******************************************************************************/

/* ---

--------------------------------------------------------------------------
### ESP8266 GPIO pin assignments

The WS2812 RGB LEDs require one output pin.

FastLED incorrectly blocks using GPIO10 so that is not an option for and LED strip.

*The ESP8266 has at most 12 usable pins but 2 (or 3) of those have existing useful functions 
which leave 9 (or 10) pins plus the single analog pin for most projects. 
(To get 10 usable pins, the RX pin is repurposed. Most projects do not receive serial data at runtime.)*
--- */

// For portability, we reference pins by their GPIO# and not the board specific identifiers.

#define DISPLAY_DC      D2 // Data/command pin for BOTH displays
#define DISPLAY_RESET   D4 // Reset pin for BOTH displays

#if 0
#define AVAILABLE_0     0
#define AVAILABLE_1     1
#define AVAILABLE_3     3
#define AVAILABLE_4     4
#define AVAILABLE_5     5
#define DO_NOT_USE_6    6
#define DO_NOT_USE_7    7
#define DO_NOT_USE_8    8
#define DO_NOT_USE_9    9
#define AVAILABLE_10    10
#define DO_NOT_USE_11   11
#define AVAILABLE_12    12
#define AVAILABLE_13    13
#define AVAILABLE_14    14
#define AVAILABLE_15    15
#define PIN_SLEEP_WAKE  16
#endif

#endif
