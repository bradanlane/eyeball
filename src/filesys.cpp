/* ***************************************************************************
* File:    filesys.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the GNU General Public License 3 (or, at your option. any later version)
*
* ******************************************************************************/

/* ---

--------------------------------------------------------------------------
### FILESYS API
Provide access to the SIFFS file system. Used for web content as well as the integrated logging system.

In addition to static web content, the API also provides interfaces for upload of new content via a web interface.
*The web upload capability requires the requisite web interface be implemented in `web.cpp`.*
--- */


#include "filesys.h"
#include <FS.h>
#include <cstdarg>
#include <stdio.h>

static File _filesys_upload_file; // a File variable to temporarily store the received file
static int _filesys_upload_size = 0;

static bool file_append(const char *path, const char *buf) {
#if 0	// this project does not need logging
	File f = SPIFFS.open(path, "a+");

	if (!f) {
		Serial.printf("SPIFF error openning %s\n", path);
		return false;
	}

	// TODO: start every line of the log with a timestamp

	uint32_t len;

#if 0	// no clock or RTC available
	// output locla clock time
	char *p = clockGetTimeAsText();
	len = strlen(p);
	for (uint32_t i = 0; i < len; i++)
		f.write(p[i]);
	f.write(':');
	f.write(' ');
#endif

	// output message buffer
	len = strlen(buf);
	for (uint32_t i = 0; i < len; i++)
		f.write(buf[i]);

	f.close();
#endif
	Serial.print(buf);

	return true;
}

static String formatBytes(size_t bytes) { // convert sizes in bytes to KB and MB
	if (bytes < 1024)
		return String(bytes) + "B";
	if (bytes < (1024 * 1024))
		return String(bytes / 1024.0) + "KB";
	if (bytes < (1024 * 1024 * 1024))
		return String(bytes / 1024.0 / 1024.0) + "MB";
	return String(bytes / 1024.0 / 1024.0 / 1024.0) + "GB";
}

/* ---
The filesys API includes an interface to access SPIFFS web content.

#### Basic File APIs
  - `filesysExists()` takes a *String* and returns a *bool* true if the provided file name exists; otherwise false
  - `filesysOpen()` takes a *String* and returns a *File* handle if the provided file name can be opened for reading; otherwise returns 0
  - `filesysClose()` takes a *File* handle and attempts to close it; returns *nothing*
--- */

bool filesysExists(String name) {
	return SPIFFS.exists(name);
}

File filesysOpen(String name) {
	return SPIFFS.open(name, "r"); // Open the file
}

void filesysClose(File handle) {
	handle.close();
}

/* ---
The filesys API includes a set of related interfaces for uploading new files.

Note: the upload operation uses internal data structures and only a single upload may be active any any given time.
No safety procedure is currently implemented to prevent multiple concurrent uploads.

#### Upload File APIs
  - `filesysSaveStart()` takes a *String* and returns a *bool* true if the provided file name can be opened for writing, otherwise returns false
  - `filesysSaveWrite()` takes a buffer and size to be written to the open file and returns *bool* true if successful, otherwise returns false
  - `filesysSaveFinish()` closes the active open file and returns *bool* true if successful, otherwise false
--- */

bool filesysSaveStart(String name) {
	if (!name.startsWith("/"))
		name = "/" + name;

	if (!name.endsWith(".gz")) {					 // The file server always prefers a compressed version of a file
		String name_as_compressed_gz = name + ".gz"; // So if an uploaded file is not compressed, the existing compressed
		if (SPIFFS.exists(name_as_compressed_gz))	// version of that file must be deleted (if it exists)
			SPIFFS.remove(name_as_compressed_gz);
	}

	_filesys_upload_size = 0;
	filesysLogPrintf(LOG_INFO, "handleFileUpload Name: %s\n", name.c_str());
	_filesys_upload_file = SPIFFS.open(name.c_str(), "w"); // Open the file for writing in SPIFFS (create if it doesn't exist)

	if (!_filesys_upload_file) {
		filesysLogPrintf(LOG_ERROR, "failed to open %s\n", name.c_str());
		return false;
	}
	return true;
}

bool filesysSaveWrite(uint8_t *buf, size_t size) {
	if (_filesys_upload_file) {
		_filesys_upload_file.write(buf, size); // Write the received bytes to the file
		_filesys_upload_size += size;
	}
	return true; // TODO check return code and/or byte count and return appropriate status
}

bool filesysSaveFinish() {
	if (_filesys_upload_file) {
		_filesys_upload_file.close(); // Close the file again
		filesysLogPrintf(LOG_INFO, "Upload %d bytes\n", _filesys_upload_size);
		_filesys_upload_size = 0;
		return true;
	}
	return false;
}


/* ---
The filesys API includes a set of logging interfaces.

Log entries are defined on a numeric scale from CRITICAL (0) and ERROR (1) to DEBUG (5). The full set of levels is defined in `filesys.h`.
The level of logging data stored in SPIFFS may be defined using `MAX_LOG_LEVEL`.
Messages will not be logged if their level is beyond the `MAX_LOG_LEVEL` defined.

#### Logging APIs
  - `filesysLogPrint()` takes an *int* log level and a *const char pointer* for the message to log.
  - `filesysLogPrintf()` takes an *int* log level and a *const char pointer* format string and a variable number of additional parameters and formats teh message to be logged similar to the `sprintf()` fucntion.
--- */

void filesysLogPrint(int level, const char *msg) {
	// log levels assend to lesser severity; 
	if (level <= MAX_LOG_LEVEL) {
		file_append("/debug.log", msg);
		//Serial.print(msg);
	}
}

static char g_fylesys_log_buffer[LOG_MAXLEN + 1];
void filesysLogPrintf(int level, const char *fmt...) {
	va_list args;

	va_start(args, fmt);

	g_fylesys_log_buffer[0] = 0;
	vsnprintf(g_fylesys_log_buffer, LOG_MAXLEN, fmt, args);
	g_fylesys_log_buffer[LOG_MAXLEN-2] = '\n';
	g_fylesys_log_buffer[LOG_MAXLEN-1] = 0;

	filesysLogPrint(level, g_fylesys_log_buffer);

	va_end(args);
}



/* ---

#### filesysInit()
Performs all necessary initialization. Must be called once before using any of the other filesys functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool filesysInit() {	  // Start the SPIFFS and list all contents
	if (SPIFFS.begin()) { // Start the SPI Flash File System (SPIFFS)
		Serial.println("SPIFFS started. Contents:");
		Dir dir = SPIFFS.openDir("/");
		while (dir.next()) { // List the file system contents
			String fileName = dir.fileName();
			size_t fileSize = dir.fileSize();
			Serial.printf("\tFile: %s, size: %s\n", fileName.c_str(), formatBytes(fileSize).c_str());
		}
		Serial.printf("\n");
		return true;
	}
	Serial.println("SPIFFS failed.");
	return false;
}

/* ---
#### filesysLoop()
Perform any periodic operations required by the SPIFFS file system.
 - return: **none**
--- */
bool filesysLoop() {
	return true;
}
