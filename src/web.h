#ifndef __WEB_H
#define __WEB_H

bool webInit();
void webLoop();
bool webSocketActive();

#endif
