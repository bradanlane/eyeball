#ifndef _WIFI_H
#define _WIFI_H

#include <Arduino.h>

bool wifiInit();
void wifiLoop();

bool otaInit();
void otaLoop();

bool wifiIsConnected();
bool wifiConnectionLost();
bool wifiIsHotspot();

char *wifiAddress();
uint8_t wifiAddressEnding();

#endif
