/* ***************************************************************************
* Project: SCREAMZY Toy
* File:    web.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### Web API
Provides the web server and websockets interfaces.

The ESP8266 creates a web server to serve the interface with is optimized for multi-touch web browsers.
The main interface provides a simulated joystick. The interface is customizable.
For details on the touch interface, see: `http://elder.ninja/locoro/page.php?page=webapp`

The web files are stored in SPIFFS. The files may be uploaded either as an spiffs.bin or
individual files may be uploaded using the web interface: `http://screamzy_xxxxxx.local/upload.html`

The UI is rendered as HTML with JavaScript and sends Json messages through the web socket interface.

The web sockets interface may be extended to add pre-programmed movement 
or even autonomous operation _if additional sensors are added to the robot_.
Add new commands to `web_socket_event()` withing the section for `WStype_TEXT`.
--- */

#include "filesys.h"
#include <ArduinoJson.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>

/* the is based from example code at: https://github.com/tttapa/ESP8266/Examples */

static ESP8266WebServer g_web_server(80); // create a web server on port 80
static WebSocketsServer g_web_socket(81); // create a websocket server on port 81
static StaticJsonDocument<512> g_json_data;
static bool g_ws_active = false;

#define WEB_WS_TIMEOUT 2000 // 2 second timeout on web socket messages
static unsigned long g_ws_timer = 0;

/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/

static String get_content_type(String filename) { // determine the filetype of a given filename, based on the extension
	if (filename.endsWith(".html"))
		return "text/html";
	else if (filename.endsWith(".css"))
		return "text/css";
	else if (filename.endsWith(".js"))
		return "application/javascript";
	else if (filename.endsWith(".ico"))
		return "image/x-icon";
	else if (filename.endsWith(".gz"))
		return "application/x-gzip";
	return "text/plain";
}

/*__________________________________________________________SERVER_HANDLERS__________________________________________________________*/

static void web_socket_event(uint8_t num, WStype_t type, uint8_t *payload, size_t length) { // When a WebSocket message is received
	switch (type) {
		case WStype_DISCONNECTED: { // if the websocket is disconnected
			Serial.printf("[%u] Disconnected!\n", num);
			//motorsFullStop();
			//ledsSetAllRGB(127, 0, 0); // signal we have a problem
			g_ws_active = false;
		} break;

		case WStype_CONNECTED: { // if a new websocket connection is established
			IPAddress ip = g_web_socket.remoteIP(num);
			Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
			//motorsFullStop();
			g_ws_active = true;
		} break;

		case WStype_TEXT: { // if new text data is received
			// Serial.printf("websocket payload: [%u] %s\n", num, payload);
			DeserializationError err = deserializeJson(g_json_data, payload);
			if (err) {
				// TODO need to return a message that we failed
				Serial.println("failed to parse the json payload");
				return;
			}

			if (g_json_data["cmd"] == "speed") {
				float left = g_json_data["left"];
				float right = g_json_data["right"];
				//motorsSpeed(left, right);
				g_ws_timer = millis() + WEB_WS_TIMEOUT; // reset the timeout timer
			} else if (g_json_data["cmd"] == "power") {
				uint16_t id = g_json_data["id"];
				float speed = g_json_data["speed"];
				//motorsPower(id, speed);
				g_ws_timer = millis() + WEB_WS_TIMEOUT; // reset the timeout timer
			} else {
				//motorsFullStop();
				Serial.println("unrecognized payload");
			}
		} break;

		default: {
			//motorsFullStop();
			Serial.println("unsupported socket event type");
		} break;
	}
}

static void handle_file_upload() { // upload a new file to the SPIFFS
	HTTPUpload &upload = g_web_server.upload();
	String path;
	if (upload.status == UPLOAD_FILE_START) {
		filesysSaveStart(upload.filename);
	} else if (upload.status == UPLOAD_FILE_WRITE) {
		filesysSaveWrite(upload.buf, upload.currentSize);
	} else if (upload.status == UPLOAD_FILE_END) {
		if (filesysSaveFinish())
			g_web_server.send(303, "text/plain", "303: success");
		else
			g_web_server.send(500, "text/plain", "500: couldn't create file");
	}
}

// we embed the upload page so it is always available, even if the spiffs.bin has not been loaded
const char g_upload_page[] PROGMEM = R"=====(
<!DOCTYPE html><html><head><title>ESP8266 Default File Upload</title></head>
<body><h1>ESP8266 SPIFFS File Upload</h1>
<p>Select a new file to upload to the ESP8266. Existing files will be replaced.</p>
<form method="POST" enctype="multipart/form-data"><input type="file" name="data"><input class="button" type="submit" value="Upload"></form>
</body></html>
)=====";

static bool handle_file_read(String path) { // send the right file to the client (if it exists)
	Serial.println("handle_file_read: " + path);
	if (path.endsWith("/"))
		path += "index.html"; // If a folder is requested, send the index file

	String content_type = get_content_type(path); // Get the MIME type
	String pathWithGz = path + ".gz";
	bool found = false;

	// If the file exists, either as a compressed archive, or normal
	if (filesysExists(pathWithGz)) { // If there's a compressed version available
		path += ".gz";				 // Use the compressed verion
		found = true;
	} else if (filesysExists(path))
		found = true;

	if (found) {
		File file = filesysOpen(path);
		g_web_server.streamFile(file, content_type); // Send it to the client
		filesysClose(file);
		// Serial.printf("sent %d bytes from %s as type %s\n", sent, path.c_str(), contentType.c_str());
		return true;
	}

	// fallback for upload if the file does not yet exist
	if (path.endsWith("upload.html")) {
		g_web_server.send(200, content_type, g_upload_page); // Send the backup upload page
	}
	Serial.printf("%s not found\n", path.c_str()); // If the file doesn't exist, return false
	return false;
}

static void handle_file_request() {				 // if the requested file or page doesn't exist, return a 404 not found error
	if (!handle_file_read(g_web_server.uri())) { // check if the file exists in the flash memory (SPIFFS), if so, send it
		g_web_server.send(404, "text/plain", "404: File Not Found");
	}
}

/*__________________________________________________________SETUP_FUNCTIONS__________________________________________________________*/

static bool start_web_server() {
	g_web_socket.begin();					// start the websocket server
	g_web_socket.onEvent(web_socket_event); // if there's an incomming websocket message, go to function 'web_socket_event'
	Serial.println("WebSocket server started");
	return true; // no way of knowing if we failed anything
}

static bool start_server() {
	// Start a HTTP server with a file read handler and an upload handler
	// If a POST request is sent to the /edit.html address,
	g_web_server.on("/upload.html", HTTP_POST, []() { g_web_server.send(200, "text/plain", ""); }, handle_file_upload); // go to 'handle_file_upload'

	// if someone requests any other file or page, go to function 'handleNotFound' and check if the file exists
	g_web_server.onNotFound(handle_file_request);

	g_web_server.begin(); // start the HTTP server
	Serial.println("HTTP server started.");
	return true; // no way ot knowing if we failed anything
}

/* ---
#### webSockedActive()
returns `bool` if teh web socket is open and active
 - return: **bool** `true` on active and `false` on inactive
--- */
bool webSocketActive() {
	return g_ws_active;
}

/* ---
#### webInit()
Performs all necessary initialization of both the webserver on port 80 and teh websockets interface on port 81.
Must be called once before using any of the other web functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool webInit() {
	g_ws_timer = 0;
	g_ws_active = false;
	// Start a WebSocket server and then HTTP server with handlers
	return (start_web_server() && start_server());
}

/* ---
#### webLoop()
Give the web interfaces an opportunity to respond to page requests and websockets messages
--- */
void webLoop() {
	g_web_socket.loop();		 // constantly check for websocket events
	g_web_server.handleClient(); // run the server

	// if the web socket has been opened then see if we have received any updates lately
	if (g_ws_active) {
		if (g_ws_timer < millis()) {
			// if the timer has run out, we stop all motors as a safety precaution
			g_ws_timer = millis() + WEB_WS_TIMEOUT; // reset the timeout timer
		}
	}
}
