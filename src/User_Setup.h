// WARNING WARNING WARNING - this is a copy of the customized file that needs to go in .piolibdeps/TFT_eSPI_ID1559/User_Setup.h

// USER DEFINED SETTINGS

#define ST7735_DRIVER      // Define additional parameters below for this display
#define TFT_WIDTH  128 // ST7789 240 x 240 and 240 x 320
#define TFT_HEIGHT 160 // ST7789 240 x 240
#define TFT_CS   PIN_D8  // Chip select control pin D8
#define TFT_DC   PIN_D3  // Data Command control pin
#define TFT_RST  PIN_D4  // Reset pin (could connect to NodeMCU RST, see next line)
#define SPI_FREQUENCY  27000000 // Actually sets it to 26.67MHz = 80/3



#define ST7789_DRIVER      // Full configuration option, define additional parameters below for this display
#define TFT_WIDTH  240 // ST7789 240 x 240 and 240 x 320
#define TFT_HEIGHT 240 // ST7789 240 x 240
#define TFT_CS   -1  // Chip select control pin D8
#define TFT_DC   PIN_D3  // Data Command control pin
#define TFT_RST  PIN_D4  // Reset pin (could connect to NodeMCU RST, see next line)
#define SPI_FREQUENCY  40000000 // Maximum to use SPIFFS



// COMMON 
#define SPI_READ_FREQUENCY  20000000
#define SPI_TOUCH_FREQUENCY  2500000
#define LOAD_GLCD   // Font 1. Original Adafruit 8 pixel font needs ~1820 bytes in FLASH
#define LOAD_FONT4  // Font 4. Medium 26 pixel high font, needs ~5848 bytes in FLASH, 96 characters
#define SMOOTH_FONT
