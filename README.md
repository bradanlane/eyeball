# eyeball

3D printed sphere with glass lens to create an eye ball with internal electronics consisting of ESP8266, ST7789 TFT display, LiPo, and charging circuit.

Initial NodeMCU prototype board wired as follows:
  - ST7789 GND to ESP8266 GND
  - ST7789 VCC to ESP8266 3.3
  - ST7789 SCL to ESP8266 D5
  - ST7789 SDA to ESP8266 D7
  - ST7789 RES to ESP8266 D4
  - ST7789 DC  to ESP8266 D3

